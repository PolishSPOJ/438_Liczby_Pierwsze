/*
 * main.cpp
 *
 *  Created on: 12.02.2017
 *      Author: Kamil
 */

#include <stdio.h>
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <cstring>

using namespace std;

typedef enum TextIndex
   {
   NO,
   YES,
   TextIndex_NumOf
   } TextIndex_T;

string TextList[TextIndex_NumOf] =
   {
   "NIE", "TAK"
   };
template<typename T>
class Value
   {
public:
   /*!
    *  \brief Primet numbers are not 0 and 1
    */
   bool isPrimeNumber();
   bool readFromStream();
protected:
private:
T value;
   };

template<typename T>
bool Value<T>::isPrimeNumber()
   {
   int Value = (int) value;
   if (Value < 2)
      {
      return false;
      }
   if(Value==2 )
      {
      return true;
      }
   else
      {
      for (int div = 2; div <= (int) sqrt(Value); div++)
         {
         if ((Value % div) == 0)
            {
            return false;
            }
         }
      }
   return true;
   }

template<typename T>
bool Value<T>::readFromStream()
   {
   cin >> value;
   return cin.fail();
   }

int main()
   {
   int numberCount = 0;
   cin >> numberCount;
   if(cin.fail())
      {
      cout << "Error" << endl;
      }
   for (int counter = 0; counter < numberCount; counter++)
      {
      Value<int> value;
      if(value.readFromStream())
         {
         cout << "Error" << endl;
         }

      if (value.isPrimeNumber())
         {
         cout << TextList[YES] << endl;
         }
      else
         {
         cout << TextList[NO] << endl;
         }
      }
   }

